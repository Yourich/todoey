//
//  Item.swift
//  Todoey
//
//  Created by Yurii Topchii on 11/18/18.
//  Copyright © 2018 Yurii Topchii. All rights reserved.
//

import Foundation
import RealmSwift

class Item : Object {
    @objc dynamic var title : String = ""
    @objc dynamic var done : Bool = false
    @objc dynamic var dateCreated : Date?
    var patentCategory = LinkingObjects(fromType: Category.self, property: "items")
}

